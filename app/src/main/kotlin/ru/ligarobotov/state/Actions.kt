package ru.ligarobotov.state

// Навигация
const val NAVIGATE_TO = "NAVIGATE_TO"
const val NAVIGATE_BACK = "NAVIGATE_BACK"
const val NAVIGATE_TO_REPLACE = "NAVIGATE_TO_REPLACE"
