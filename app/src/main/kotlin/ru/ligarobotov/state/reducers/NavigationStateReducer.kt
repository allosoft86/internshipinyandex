package ru.ligarobotov.state.reducers

import ru.ligarobotov.redux.Action
import ru.ligarobotov.state.NAVIGATE_BACK
import ru.ligarobotov.state.NAVIGATE_TO
import ru.ligarobotov.state.NAVIGATE_TO_REPLACE

val navigationStateReducer = { navigationStack: List<Int>, action: Action ->
    when(action.type) {
        NAVIGATE_TO -> {
            val newItem = (action.payload as Int)
            if (navigationStack.isEmpty() || navigationStack.last() != newItem) {
                navigationStack + newItem
            } else {
                navigationStack
            }
        }
        NAVIGATE_TO_REPLACE -> {
            val newItem = (action.payload as Int)
            if (!navigationStack.isEmpty() && navigationStack.last() != newItem) {
                navigationStack.take(navigationStack.size - 1) + newItem
            } else {
                navigationStack
            }
        }

        NAVIGATE_BACK -> navigationStack.dropLast(1)
        else -> navigationStack
    }
}