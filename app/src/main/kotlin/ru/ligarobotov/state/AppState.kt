package ru.ligarobotov.state

data class AppState(val navigationStack: List<Int>)