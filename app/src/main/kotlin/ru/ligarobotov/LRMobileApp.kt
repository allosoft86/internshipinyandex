package ru.ligarobotov

import android.app.Application
import android.util.Log
import ru.ligarobotov.redux.Action
import ru.ligarobotov.redux.Middleware
import ru.ligarobotov.redux.Store
import ru.ligarobotov.redux.combineReducers
import ru.ligarobotov.state.AppState
import ru.ligarobotov.state.reducers.navigationStateReducer

class LRMobileApp : Application() {

    val TAG = "LRMobileApp"

    val store = Store(AppState(emptyList()),
        combineReducers(
            AppState::navigationStack, navigationStateReducer,
            ::AppState
        )
    )

    override fun onCreate() {
        super.onCreate()

        store.addMiddleware(object : Middleware<AppState> {
            override fun dispatch(
                store: Store<AppState>,
                nextDispatcher: (action: Action) -> Unit
            ) = { action: Action ->
                Log.d(TAG, "State: ${store.state}")
                Log.d(TAG, "Action: ${action.type}, payload: ${action.payload}")
                nextDispatcher(action)
            }
        })
    }
}