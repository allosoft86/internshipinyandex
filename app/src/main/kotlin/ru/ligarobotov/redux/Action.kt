package ru.ligarobotov.redux

/**
 * Класс действия из архитектуры Redux. Хранит тип и объект (необязвательно)
 */
data class Action(val type: String, val payload: Any? = null)