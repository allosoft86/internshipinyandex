package ru.ligarobotov.redux

/**
 * Интерфейс редьюсера в архитектуре Redux.
 */
typealias Reducer<State> = (state: State, action: Action) -> State

/**
 * Комбинация нескольких редьюсеров. Каждый комбинируемый редьюсер принимает State и возвращает State.
 */
fun <State : Any> combineReducers(vararg reducers: Reducer<State>) : Reducer<State> {
    return { startState: State, action: Action ->
        reducers.fold(startState) { state, reducer ->
            reducer(state, action)
        }
    }
}

/**
 * Комбинация нескольких редьюсеров с применением отображения.
 * Каждый комбинируемый редьюсер принимает подстостояние SubState и возвращает Subsctate.
 * Итоговое состояние State строится с помощью функции builder.
 */
fun <State : Any,
        SubState1 : Any> combineReducers(mapper1: (State) -> SubState1, reducer1: Reducer<SubState1>,
                                         builder: (SubState1) -> State) : Reducer<State> {
    return { startState: State, action: Action ->
        builder(
                reducer1(mapper1(startState), action)
        )
    }
}

fun <State : Any,
        SubState1 : Any,
        SubState2 : Any> combineReducers(mapper1: (State) -> SubState1, reducer1: Reducer<SubState1>,
                                         mapper2: (State) -> SubState2, reducer2: Reducer<SubState2>,
                                         builder: (SubState1, SubState2) -> State) : Reducer<State> {
    return { startState: State, action: Action ->
        builder(
                reducer1(mapper1(startState), action),
                reducer2(mapper2(startState), action)
        )
    }
}

fun <State : Any,
        SubState1 : Any,
        SubState2 : Any,
        SubState3 : Any> combineReducers(mapper1: (State) -> SubState1, reducer1: Reducer<SubState1>,
                                         mapper2: (State) -> SubState2, reducer2: Reducer<SubState2>,
                                         mapper3: (State) -> SubState3, reducer3: Reducer<SubState3>,
                                         builder: (SubState1, SubState2, SubState3) -> State) : Reducer<State> {
    return { startState: State, action: Action ->
        builder(
                reducer1(mapper1(startState), action),
                reducer2(mapper2(startState), action),
                reducer3(mapper3(startState), action)
        )
    }
}

fun <State : Any,
        SubState1 : Any,
        SubState2 : Any,
        SubState3 : Any,
        SubState4 : Any> combineReducers(mapper1: (State) -> SubState1, reducer1: Reducer<SubState1>,
                                         mapper2: (State) -> SubState2, reducer2: Reducer<SubState2>,
                                         mapper3: (State) -> SubState3, reducer3: Reducer<SubState3>,
                                         mapper4: (State) -> SubState4, reducer4: Reducer<SubState4>,
                                         builder: (SubState1, SubState2, SubState3, SubState4) -> State) : Reducer<State> {
    return { startState: State, action: Action ->
        builder(
                reducer1(mapper1(startState), action),
                reducer2(mapper2(startState), action),
                reducer3(mapper3(startState), action),
                reducer4(mapper4(startState), action)
        )
    }
}

fun <State : Any,
        SubState1 : Any,
        SubState2 : Any,
        SubState3 : Any,
        SubState4 : Any,
        SubState5 : Any> combineReducers(mapper1: (State) -> SubState1, reducer1: Reducer<SubState1>,
                                         mapper2: (State) -> SubState2, reducer2: Reducer<SubState2>,
                                         mapper3: (State) -> SubState3, reducer3: Reducer<SubState3>,
                                         mapper4: (State) -> SubState4, reducer4: Reducer<SubState4>,
                                         mapper5: (State) -> SubState5, reducer5: Reducer<SubState5>,
                                         builder: (SubState1, SubState2, SubState3, SubState4, SubState5) -> State) : Reducer<State> {
    return { startState: State, action: Action ->
        builder(
                reducer1(mapper1(startState), action),
                reducer2(mapper2(startState), action),
                reducer3(mapper3(startState), action),
                reducer4(mapper4(startState), action),
                reducer5(mapper5(startState), action)
        )
    }
}
