package ru.ligarobotov.redux

/**
 * Класс Хранилища (Store) в архитектуре Redux.
 *
 */
class Store<State : Any>(initialState: State,
                         val reducer: Reducer<State>) {
    var state = initialState
        private set
    private val middlewares = mutableListOf<Middleware<State>>()
    private val subscriptions = mutableMapOf<Any, MutableList<Pair<(State) -> Any, Any>>>()
    private val actions = mutableListOf<Action>()

    /**
     * Метод, добавляющий перехватчик в цепочку
     */
    fun addMiddleware(middleware: Middleware<State>) {
        middlewares.add(middleware)
    }

    /**
     * Метод, позволяющий подписаться на обновление подсостояния
     * context - некий объект, к которому привязывается подписка. Используется при отписке
     */
    fun <Context : Any, M : Any> subscribe(context: Context,
                                           mapper: (State) -> M,
                                           subscriber: (Context, M) -> Unit) {
        // TODO: queue subscribe action
        val subscriptionsList = subscriptions[context] ?: mutableListOf()
        val pair = Pair(mapper, subscriber)
        subscriptionsList.add(pair)
        subscriptions[context] = subscriptionsList
        subscriber(context, mapper(state))
    }

    /**
     * Метод, позволяющий подписаться на обновление состояния
     * context - некий объект, к которому привязывается подписка. Используется при отписке
     */
    fun <Context : Any> subscribe(context: Context,
                                  subscriber: (Context, State) -> Unit) =
            subscribe(context, {state -> state}, subscriber)

    /**
     * Отписка для определённого контекста
     */
    fun unsubscribe(context: Any) {
        // TODO: queue unsubscribe action
        subscriptions.remove(context)
    }

    /**
     * Отправка действия в хранилище. Вызывает помледовательную обработку цепочкой перехватчиков (middlewares).
     * Если действие не будет перехвачено, вызываются обработчики для тех подписчиков, подсостояния для которых изменились
     */
    fun dispatch(action: Action) {
        actions.add(action)
        if (actions.size == 1) {
            processAction(action)
        }

    }

    private fun processAction(action: Action) {
        val initialDispatcher = { resultingAction : Action ->
            val prevState = state
            state = reducer(prevState, resultingAction)
            for ((context, subscriptionList) in subscriptions) {
                for (subscription in subscriptionList) {
                    try {
                        val value = subscription.first.invoke(state)
                        val prevValue = subscription.first.invoke(prevState)
                        if (value !== prevValue) {
                            callSubscription(context, value, subscription.second)
                        }
                    } catch (error: NullPointerException) {
                        //
                    }
                }
            }
            Unit
        }
        middlewares.foldRight(initialDispatcher) { middleware, dispatcher ->
            middleware.dispatch(this, dispatcher)
        }.invoke(action)
        actions.removeAt(0)
        if (actions.size > 0) {
            processAction(actions[0])
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun <Context : Any, M : Any> callSubscription(context: Context,
                                                   m: M,
                                                   subscription: Any) {
        (subscription as (Context, M) -> Unit).invoke(context, m)
    }
}
