package ru.ligarobotov.redux

/**
 * Интерфейс для перехватчика (middleware) в архитектуре Redux.
 * Позволяет перехватывать действия, посылаемые в Хранилище (Store) и обрабатывать их.
 */
interface Middleware<State : Any> {
    fun dispatch(store: Store<State>, nextDispatcher: (action: Action) -> Unit): (action: Action) -> Unit
}