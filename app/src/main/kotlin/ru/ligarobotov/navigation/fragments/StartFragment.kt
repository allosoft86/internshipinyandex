package ru.ligarobotov.navigation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_start.*
import ru.ligarobotov.R
import ru.ligarobotov.navigation.BaseFragment
import ru.ligarobotov.redux.Action
import ru.ligarobotov.state.NAVIGATE_TO

class StartFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_start, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnInfo.setOnClickListener{
            getStore().dispatch(Action(NAVIGATE_TO, R.id.infoFragment))
        }

        btnLogin.setOnClickListener{
            getStore().dispatch(Action(NAVIGATE_TO, R.id.otpConfirmFragment))
        }


    }
}