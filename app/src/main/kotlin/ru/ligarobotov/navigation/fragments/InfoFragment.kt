package ru.ligarobotov.navigation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_info.*
import ru.ligarobotov.R
import ru.ligarobotov.navigation.BaseFragment
import ru.ligarobotov.redux.Action
import ru.ligarobotov.state.NAVIGATE_TO


class InfoFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnEnroll.setOnClickListener{
            getStore().dispatch(Action(NAVIGATE_TO, R.id.enrollFragment))
        }

    }
}