package ru.ligarobotov.navigation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.activity_main.*
import ru.ligarobotov.LRMobileApp
import ru.ligarobotov.R
import ru.ligarobotov.redux.Action
import ru.ligarobotov.state.AppState
import ru.ligarobotov.state.NAVIGATE_BACK
import ru.ligarobotov.state.NAVIGATE_TO
import ru.ligarobotov.state.NAVIGATE_TO_REPLACE
import kotlin.math.min

class MainActivity : AppCompatActivity() {
    private var navController: NavController? = null
    private var cacheNavigationStack: List<Int> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottom_navigation.setOnNavigationItemSelectedListener(::onTeacherMenuItemSelected)

        navController = Navigation.findNavController(this, R.id.navigation_host_fragment)

        getStore().subscribe(this, AppState::navigationStack) { activity, navigationStack ->
            if (navigationStack.isEmpty()) {
                cacheNavigationStack = navigationStack
                // особый случай при старте приложения - стек ещё не проинициализирован
                if (navController?.currentDestination != null && navController?.currentDestination?.id != 0) {
                    activity.getStore().dispatch(Action(NAVIGATE_TO, navController?.currentDestination?.id))
                } else {
                    activity.getStore().dispatch(Action(NAVIGATE_TO, R.id.startFragment))
                }
            } else if (navigationStack.size == 1 && navigationStack[0] == navController?.currentDestination?.id) {
                // Особый случай при старте приложения: фрагмент уже фактически на стеке (виден),
                // но его ещё не было в состоянии
                cacheNavigationStack = navigationStack
            } else {
                var matchFragmentsCount = min(navigationStack.size, cacheNavigationStack.size)
                for ((i, targetId) in cacheNavigationStack.withIndex()) {
                    if (i >= navigationStack.size) {
                        navController?.popBackStack(targetId, true)
                        break
                    } else if (targetId != navigationStack[i]) {
                        navController?.popBackStack(targetId, true)
                        matchFragmentsCount = i
                        break
                    }
                }
                for (i in (matchFragmentsCount until navigationStack.size)) {
                    navController?.navigate(navigationStack[i])
                }
                cacheNavigationStack = navigationStack
            }

            toggleNavigationBarIfNeeded(cacheNavigationStack)
        }
    }

    override fun onDestroy() {
        getStore().unsubscribe(this)
        navController = null
        cacheNavigationStack = emptyList()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (cacheNavigationStack.size > 1) {
            getStore().dispatch(Action(NAVIGATE_BACK))
        } else {
            super.onBackPressed()
        }
    }

    private fun getStore() = (applicationContext as LRMobileApp).store

    private fun toggleNavigationBarIfNeeded(navigationStack: List<Int>) {
        if (navigationStack.isEmpty()) {
            bottom_navigation.visibility = View.GONE
            return
        }

        bottom_navigation.visibility = View.VISIBLE
        when (navigationStack.last()) {
            R.id.scheduleFragment -> bottom_navigation.selectedItemId = R.id.scheduleItem
            R.id.mainTeacherFragment -> bottom_navigation.selectedItemId = R.id.mainTeacherItem
            R.id.newsFragment -> bottom_navigation.selectedItemId = R.id.newsItem
            R.id.filialsFragment -> bottom_navigation.selectedItemId = R.id.filialsItem
            R.id.photoFragment -> bottom_navigation.selectedItemId = R.id.photoItem
            else -> bottom_navigation.visibility = View.GONE
        }
    }

    private fun onTeacherMenuItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.scheduleItem -> getStore().dispatch(Action(NAVIGATE_TO_REPLACE, R.id.scheduleFragment))
            R.id.mainTeacherItem -> getStore().dispatch(Action(NAVIGATE_TO_REPLACE, R.id.mainTeacherFragment))
            R.id.newsItem -> getStore().dispatch(Action(NAVIGATE_TO_REPLACE, R.id.newsFragment))
            R.id.filialsItem -> getStore().dispatch(Action(NAVIGATE_TO_REPLACE, R.id.filialsFragment))
            R.id.photoItem -> getStore().dispatch(Action(NAVIGATE_TO_REPLACE, R.id.photoFragment))
        }

        return true
    }

}
