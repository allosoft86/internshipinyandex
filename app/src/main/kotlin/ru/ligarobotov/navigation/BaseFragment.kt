package ru.ligarobotov.navigation

import android.support.v4.app.Fragment
import ru.ligarobotov.LRMobileApp
import ru.ligarobotov.state.AppState

open class BaseFragment : Fragment() {
    override fun onDestroyView() {
        super.onDestroyView()

        getStore().unsubscribe(this)
    }

    protected fun <M : Any> subscribe(mapper: (AppState) -> M,
                                                     subscriber: (BaseFragment, M) -> Unit) = getStore().subscribe(this, mapper, subscriber)
    protected fun getStore() = (activity!!.applicationContext as LRMobileApp).store
}