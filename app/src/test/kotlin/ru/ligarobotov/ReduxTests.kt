package ru.ligarobotov

import ru.ligarobotov.redux.*
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import kotlin.test.*

class ReduxTests {
    private var store: Store<Int> = Store(0) { state: Int, _ -> state }

    @BeforeTest
    fun initStore() {
        store = Store(0) { state: Int, action: Action ->
            when (action.type) {
                "inc" -> state + 1
                "dec" -> state - 1
                "plus" -> state + action.payload as Int
                else -> state
            }
        }
    }

    @Test
    fun storeInit() {
        assertEquals(0, store.state)
    }

    @Test
    fun sendAction() {
        store.dispatch(Action("inc"))
        assertEquals(1, store.state)
        store.dispatch(Action("plus", 3))
        assertEquals(4, store.state)
        store.dispatch(Action("dec"))
        assertEquals(3, store.state)
    }

    @Test
    fun middleware() {
        store.addMiddleware(object : Middleware<Int> {
            override fun dispatch(
                store: Store<Int>,
                nextDispatcher: (action: Action) -> Unit
            ) = { action: Action ->
                when (action.type) {
                    "inc" -> nextDispatcher.invoke(Action("dec"))
                    "dec" -> Unit
                    else -> nextDispatcher.invoke(action)
                }
                Unit
            }
        })
        store.dispatch(Action("inc"))
        assertEquals(-1, store.state)
        store.dispatch(Action("plus", 3))
        assertEquals(2, store.state)
        store.dispatch(Action("dec"))
        assertEquals(2, store.state)
    }

    @Test
    fun middlewares2() {
        store.addMiddleware(object : Middleware<Int> {
            override fun dispatch(
                store: Store<Int>,
                nextDispatcher: (action: Action) -> Unit
            ) = { action: Action ->
                when (action.type) {
                    "inc" -> nextDispatcher.invoke(Action("dec"))
                    "dec" -> Unit
                    else -> nextDispatcher.invoke(action)
                }
                Unit
            }
        })
        store.addMiddleware(object : Middleware<Int> {
            override fun dispatch(
                store: Store<Int>,
                nextDispatcher: (action: Action) -> Unit
            ) = { action: Action ->
                when (action.type) {
                    "dec" -> nextDispatcher.invoke(Action("plus", store.state))
                    else -> nextDispatcher.invoke(action)
                }
                Unit
            }
        })
        store.dispatch(Action("plus", 3))
        assertEquals(3, store.state)
        store.dispatch(Action("inc"))
        assertEquals(6, store.state)
        store.dispatch(Action("dec"))
        assertEquals(6, store.state)
    }

    @Test
    fun subscription() {
        val initialCall = CompletableFuture<Boolean>()
        val changeCall = CompletableFuture<Boolean>()
        val wrongCall = CompletableFuture<Boolean>()
        store.subscribe(this) { _, state ->
            when (state) {
                0 -> initialCall.complete(true)
                3 -> changeCall.complete(true)
                else -> wrongCall.complete(true)
            }
        }

        assertTrue(initialCall.get(500, TimeUnit.MILLISECONDS))

        store.dispatch(Action("plus", 3))
        assertTrue(changeCall.get(500, TimeUnit.MILLISECONDS))

        assertFailsWith(TimeoutException::class) {
            wrongCall.get(500, TimeUnit.MILLISECONDS)
        }
    }

    @Test
    fun unsubscription() {
        val initialCall = CompletableFuture<Boolean>()
        val changeCall = CompletableFuture<Boolean>()
        val wrongCall = CompletableFuture<Boolean>()
        store.subscribe(this) { _, state ->
            when (state) {
                0 -> initialCall.complete(true)
                3 -> changeCall.complete(true)
                else -> wrongCall.complete(true)
            }
        }

        assertTrue(initialCall.get(500, TimeUnit.MILLISECONDS))

        store.dispatch(Action("plus", 3))
        assertTrue(changeCall.get(500, TimeUnit.MILLISECONDS))

        assertFailsWith(TimeoutException::class) {
            wrongCall.get(500, TimeUnit.MILLISECONDS)
        }

        store.unsubscribe(this)
        store.dispatch(Action("plus", 3))
        assertFailsWith(TimeoutException::class) {
            wrongCall.get(500, TimeUnit.MILLISECONDS)
        }
    }

    data class TestState(val a: Int, val b: Int)

    @Test
    fun combineReducers() {
        val reducer = combineReducers<TestState>({ state, action ->
            when (action.type) {
                "incA" -> state.copy(a = state.a + 1)
                "swap" -> state.copy(a = state.b, b = state.a)
                else -> state
            }
        },
            { state, action ->
                when (action.type) {
                    "incB" -> state.copy(b = state.b + 1)
                    else -> state
                }
            })
        val store = Store(TestState(1, 100), reducer)

        store.dispatch(Action("incA"))
        assertEquals(2, store.state.a)
        assertEquals(100, store.state.b)
        store.dispatch(Action("incB"))
        assertEquals(2, store.state.a)
        assertEquals(101, store.state.b)
        store.dispatch(Action("swap"))
        assertEquals(101, store.state.a)
        assertEquals(2, store.state.b)
    }

    @Test
    fun combineReducersWithMappers() {
        val reducerA = { a: Int, action: Action ->
            when (action.type) {
                "incA" -> a + 1
                else -> a
            }
        }

        val reducerB = { b: Int, action: Action ->
            when (action.type) {
                "incB" -> b + 1
                else -> b
            }
        }

        val reducerAll = { state: TestState, action: Action ->
            when (action.type) {
                "swap" -> state.copy(b = state.a, a = state.b)
                else -> state
            }
        }

        val reducer = combineReducers(
            TestState::a, reducerA,
            TestState::b, reducerB,
            ::TestState
        )
        val store = Store(TestState(1, 100), combineReducers(reducerAll, reducer))

        store.dispatch(Action("incA"))
        assertEquals(2, store.state.a)
        assertEquals(100, store.state.b)
        store.dispatch(Action("incB"))
        assertEquals(2, store.state.a)
        assertEquals(101, store.state.b)
        store.dispatch(Action("swap"))
        assertEquals(101, store.state.a)
        assertEquals(2, store.state.b)
    }

    @Test
    fun subscriptionWithMappers() {
        val reducer = { state: TestState, action: Action ->
            when (action.type) {
                "incA" -> state.copy(a = state.a + 1, b = state.b)
                "incB" -> state.copy(a = state.a, b = state.b + 1)
                "swap" -> state.copy(b = state.a, a = state.b)
                else -> state
            }
        }

        val store = Store(TestState(1, 100), reducer)

        val initialCallA = CompletableFuture<Boolean>()
        val changeCallA = CompletableFuture<Boolean>()
        val wrongCallA = CompletableFuture<Boolean>()
        val initialCallB = CompletableFuture<Boolean>()
        val changeCallB = CompletableFuture<Boolean>()
        val wrongCallB = CompletableFuture<Boolean>()
        var counter = 0
        store.subscribe(this, TestState::a) { _, state ->
            when (state) {
                1 -> initialCallA.complete(true)
                2 -> changeCallA.complete(true)
                else -> wrongCallA.complete(true)
            }
        }

        store.subscribe(this, TestState::b) { _, state ->
            when (state) {
                100 -> initialCallB.complete(true)
                101 -> changeCallB.complete(true)
                else -> wrongCallB.complete(true)
            }
        }

        store.subscribe(this) { _, _ ->
            counter++
        }

        assertTrue(initialCallA.get(500, TimeUnit.MILLISECONDS))
        assertTrue(initialCallB.get(500, TimeUnit.MILLISECONDS))

        store.dispatch(Action("incA"))
        assertTrue(changeCallA.get(500, TimeUnit.MILLISECONDS))
        assertFailsWith(TimeoutException::class) {
            wrongCallB.get(100, TimeUnit.MILLISECONDS)
        }

        store.dispatch(Action("incB"))
        assertTrue(changeCallB.get(500, TimeUnit.MILLISECONDS))
        assertFailsWith(TimeoutException::class) {
            wrongCallA.get(100, TimeUnit.MILLISECONDS)
        }
        assertEquals(3, counter)
    }
}

